package qrpc

import (
	"bitbucket.org/moelius/qrpc/broker"
	"bitbucket.org/moelius/qrpc/concurrency"
)

type RPCOption func(options *rpcOptions)

type rpcOptions struct {
	maxWorkers int
}

var defaultRPCOptions = rpcOptions{
	maxWorkers: 1,
}

type RPC struct {
	server  *Server
	pool    *concurrency.Pool
	options rpcOptions
}

func (server *Server) NewRPC(handler interface{}, responseWriter interface{}, options ...RPCOption) (rpc *RPC) {
	opts := defaultRPCOptions
	for _, o := range options {
		o(&opts)
	}
	rpc = &RPC{
		server:  server,
		pool:    concurrency.NewPool(handler, responseWriter, opts.maxWorkers),
		options: opts,
	}
	server.registry = append(server.registry, rpc)
	return
}

func (rpc *RPC) NewConsumer(opts interface{}) (err error) {
	err = broker.NewConsumer(rpc.server.broker, opts, rpc.pool)
	return
}

func (rpc *RPC) NewProducer(opts interface{}) (err error) {
	err = broker.NewProducer(rpc.server.broker, opts)
	return
}

func MaxWorkers(max int) RPCOption {
	return func(o *rpcOptions) {
		o.maxWorkers = max
	}
}
