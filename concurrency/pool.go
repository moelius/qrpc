package concurrency

import (
	"log"
	"reflect"
	"sync"
)

type Pool struct {
	RequestHandler reflect.Value
	ResponseWriter reflect.Value
	MaxWorkers     int
	RequestChan    chan interface{}
	ResponseChan   chan interface{}
	stopChan       chan bool
	workers        []struct{ StopChan chan bool }
	mu             *sync.Mutex
	wg             sync.WaitGroup
}

func NewPool(handler interface{}, responseWriter interface{}, maxWorkers int) (pool *Pool) {
	return &Pool{
		RequestHandler: reflect.ValueOf(handler),
		ResponseWriter: reflect.ValueOf(responseWriter),
		MaxWorkers:     maxWorkers,
		RequestChan:    make(chan interface{}),
		ResponseChan:   make(chan interface{}),
		stopChan:       make(chan bool),
		mu:             &sync.Mutex{},
		wg:             sync.WaitGroup{},
	}
}

func (pool *Pool) Start() (err error) {
	for i := 0; i < pool.MaxWorkers; i++ {
		pool.wg.Add(1)
		go func(i int) {
			worker := struct{ StopChan chan bool }{StopChan: make(chan bool)}
			pool.mu.Lock()
			pool.workers = append(pool.workers, worker)
			pool.mu.Unlock()
			log.Printf("worker started [%d]", i)
			for {
				select {
				case task := <-pool.RequestChan:
					val := pool.RequestHandler.Call([]reflect.Value{reflect.ValueOf(task)})
					if len(val) > 1 {
						var args []interface{}
						for _, v := range val {
							args = append(args, v.Interface())
						}
						pool.ResponseChan <- pool.ResponseWriter.Call([]reflect.Value{reflect.ValueOf(args)})
					} else if len(val) == 1 {
						arg := val[0].Interface()
						pool.ResponseChan <- pool.ResponseWriter.Call([]reflect.Value{reflect.ValueOf(arg)})[0].Interface()
					}
				case <-worker.StopChan:
					log.Printf("worker stoped [%d]", i)
					pool.wg.Done()
					return
				}
			}
		}(i)
	}
	go func() {
		<-pool.stopChan
		for _, worker := range pool.workers {
			worker.StopChan <- true
		}
	}()
	return
}

func (pool *Pool) Stop() {
	pool.stopChan <- true
	pool.wg.Wait()
	log.Printf("pool stoped")
}
