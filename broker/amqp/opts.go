package amqp

import amqpp "github.com/streadway/amqp"

type Publishing = amqpp.Publishing

type Delivery = amqpp.Delivery

type Table = amqpp.Table

var (
	ExchangeDirect = amqpp.ExchangeDirect
	ExchangeFanout = amqpp.ExchangeFanout
	ExchangeTopic  = amqpp.ExchangeTopic
)

type ConsumerOptions struct {
	ConsumerTag    string
	AutoAck        bool
	Exclusive      bool
	NoLocal        bool
	NoWait         bool
	Args           Table
	ChannelOptions *ChannelOpts
	QosOptions     *QosOpts
	BindingOptions *QueueBindingOpts
}

type ProducerOptions struct {
	ChannelOptions *ChannelOpts
	QosOptions     *QosOpts
}

type QosOpts struct {
	PrefetchCount int
	PrefetchSize  int
	Global        bool
}

type QueueOpts struct {
	Name       string
	Durable    bool
	AutoDelete bool
	Exclusive  bool
	NoWait     bool
	Args       Table
}

type QueueBindingOpts struct {
	Key    string
	Queue  *QueueOpts
	NoWait bool
	Args   Table
}

type ChannelOpts struct {
	Exchange     string
	ExchangeType string
	Durable      bool
	AutoDelete   bool
	Internal     bool
	NoWait       bool
	Args         Table
}

type PublishingOpts struct {
	Exchange   string
	RoutingKey string
	Mandatory  bool
	Immediate  bool
	Data       Publishing
}
