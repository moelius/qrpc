package amqp

import (
	"github.com/streadway/amqp"
	"log"
	"sync"
	"time"
)

type Connection struct {
	url string
	sync.RWMutex
	amqp             *amqp.Connection
	state            int
	notify           chan *amqp.Error
	maxReconnects    int
	reconnectTimeout time.Duration
}

func NewConnection(connectionURL string) (connection *Connection) {
	return &Connection{url: connectionURL}
}

func (conn *Connection) Open() (err error) {
	if conn.amqp, err = amqp.Dial(conn.url); err != nil {
		return
	}
	conn.notify = conn.amqp.NotifyClose(make(chan *amqp.Error))
	conn.setState(opened)
	return
}

func (conn *Connection) Close() (err error) {
	if conn.getState() == closed {
		return
	}
	conn.setState(closing)
	if err = conn.amqp.Close(); err != nil {
		return
	}
	return
}

func (conn *Connection) Reconnect() (err error) {
	if conn.getState() == opened {
		return
	}
	ticker := time.NewTicker(conn.reconnectTimeout)
	reconCounter := 0
	for range ticker.C {
		reconCounter++
		err = conn.Open()
		if err != nil {
			log.Printf("reconnection error=[%s]", err)
			if reconCounter == conn.maxReconnects {
				break
			}
			continue
		}
		break
	}
	ticker.Stop()
	return
}

func (conn *Connection) getState() int {
	conn.RLock()
	defer conn.RUnlock()
	return conn.state
}

func (conn *Connection) setState(state int) {
	conn.Lock()
	conn.state = state
	conn.Unlock()
}
