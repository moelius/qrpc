package amqp

import (
	"bitbucket.org/moelius/qrpc/concurrency"
	"fmt"
	"github.com/streadway/amqp"
	"log"
	"sync"
)

type Consumer struct {
	sync.RWMutex
	broker       *Broker
	conn         *Connection
	channel      *amqp.Channel
	deliveryChan <-chan amqp.Delivery
	publishChan  chan amqp.Publishing
	options      *ConsumerOptions
	pool         *concurrency.Pool
	notifyClose  chan *amqp.Error
	state        int
}

func (c *Consumer) Start() (err error) {
	channel, err := c.conn.amqp.Channel()
	if err != nil {
		return
	}
	if err = channel.ExchangeDeclare(
		c.options.ChannelOptions.Exchange,
		c.options.ChannelOptions.ExchangeType,
		c.options.ChannelOptions.Durable,
		c.options.ChannelOptions.AutoDelete,
		c.options.ChannelOptions.Internal,
		c.options.ChannelOptions.NoWait,
		c.options.ChannelOptions.Args,
	); err != nil {
		return fmt.Errorf("error while exchange declare [%s]", err)
	}
	queue, err := channel.QueueDeclare(
		c.options.BindingOptions.Queue.Name,
		c.options.BindingOptions.Queue.Durable,
		c.options.BindingOptions.Queue.AutoDelete,
		c.options.BindingOptions.Queue.Exclusive,
		c.options.BindingOptions.Queue.NoWait,
		c.options.BindingOptions.Queue.Args,
	)
	if err != nil {
		return fmt.Errorf("error while queue declare [%s]", err)
	}
	if c.options.QosOptions != nil {
		err = channel.Qos(c.options.QosOptions.PrefetchCount, c.options.QosOptions.PrefetchSize, c.options.QosOptions.Global)
		if err != nil {
			return fmt.Errorf("error while setting `qos` [%s]", err)
		}
	}
	if err = channel.QueueBind(
		queue.Name,
		c.options.BindingOptions.Key,
		c.options.ChannelOptions.Exchange,
		c.options.BindingOptions.NoWait,
		c.options.BindingOptions.Args,
	); err != nil {
		return fmt.Errorf("error while queue bind [%s]", err)
	}
	c.deliveryChan, err = channel.Consume(
		queue.Name,
		c.options.ConsumerTag,
		c.options.AutoAck,
		c.options.Exclusive,
		c.options.NoLocal,
		c.options.NoWait,
		c.options.Args,
	)
	if err != nil {
		return fmt.Errorf("can't consume from queue [%s], error=[%s]", queue.Name, err)
	}
	c.channel = channel
	c.setState(opened)
	c.notifyClose = make(chan *amqp.Error)
	stopChan := c.startConsume()
	c.channelWatcher(stopChan)
	return
}

func (c *Consumer) Stop() (err error) {
	if c.getState() == opened {
		err = c.channel.Close()
	}
	return
}

func (c *Consumer) channelWatcher(stopChan chan bool) {
	go func() {
		<-c.channel.NotifyClose(c.notifyClose)
		stopChan <- true
		<-stopChan
		close(stopChan)
		log.Println("consumer stopped")
		c.setState(closed)
	}()
}

func (c *Consumer) startConsume() (stopChan chan bool) {
	stopChan = make(chan bool)
	pubStopChan := func() (stopChan chan bool) {
		stopChan = make(chan bool)
		go func() {
		Loop:
			for {
				select {
				case pubOptsInterface := <-c.pool.ResponseChan:
					if pubOptsInterface != nil {
						if e := c.broker.Publish(pubOptsInterface); e != nil {
							log.Println(e)
						}
					}
				case <-stopChan:
					break Loop
				}
			}
			stopChan <- true
		}()
		return
	}()
	go func(pStopChan chan bool) {
	Loop:
		for {
			select {
			case delivery, ok := <-c.deliveryChan:
				if !ok {
					continue Loop
				}
				if delivery.RoutingKey == c.options.BindingOptions.Key {
					c.pool.RequestChan <- delivery
				} else {
					cName := fmt.Sprintf("%s.%s.%s.%s",
						c.options.ChannelOptions.Exchange,
						c.options.ChannelOptions.ExchangeType,
						c.options.BindingOptions.Queue.Name,
						delivery.RoutingKey,
					)
					iExtConsumer, ok := c.broker.consumers.Load(cName)
					if !ok {
						log.Fatal(fmt.Sprintf("have no consumer in registry with name=[%s]", cName))
					}
					iExtConsumer.(*Consumer).pool.RequestChan <- delivery
				}
				if !c.options.AutoAck {
					if e := delivery.Ack(false); e != nil {
						log.Println(e)
					}
				}
			case <-stopChan:
				pStopChan <- true
				<-pStopChan
				break Loop
			}
		}
		stopChan <- true
	}(pubStopChan)

	return stopChan
}

func (c *Consumer) getState() int {
	c.RLock()
	defer c.RUnlock()
	return c.state
}

func (c *Consumer) setState(state int) {
	c.Lock()
	c.state = state
	c.Unlock()
}
