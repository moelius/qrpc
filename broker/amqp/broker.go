package amqp

import (
	"bitbucket.org/moelius/qrpc/broker"
	"bitbucket.org/moelius/qrpc/concurrency"
	"fmt"
	"github.com/streadway/amqp"
	"log"
	"sync"
	"syscall"
)

const (
	//states
	closed = iota
	opened
	closing
)

type Broker struct {
	options   *broker.Options
	inConn    *Connection //Incoming connection
	outConn   *Connection //Outgoing connection
	consumers sync.Map    //safe store for consumers
	producers sync.Map    //safe store for producers
	stopChan  chan bool   //Broker stopChan channel
}

func NewBroker() (b *Broker) {
	return &Broker{
		stopChan: make(chan bool),
	}
}

func (b *Broker) Configure(options broker.Options) error {
	b.options = &options
	b.inConn = NewConnection(b.options.URL)
	b.outConn = NewConnection(b.options.URL)
	return nil
}

func (b *Broker) NewConsumer(opts interface{}, pool *concurrency.Pool) (err error) {
	options, ok := opts.(ConsumerOptions)
	if !ok {
		return fmt.Errorf("wrong type of `consumer options`, you must use `amqp.consumerOptions` struct")
	}
	cName := fmt.Sprintf("%s.%s.%s.%s",
		options.ChannelOptions.Exchange,
		options.ChannelOptions.ExchangeType,
		options.BindingOptions.Queue.Name,
		options.BindingOptions.Key,
	)
	b.consumers.Store(cName, &Consumer{
		broker:      b,
		pool:        pool,
		publishChan: make(chan amqp.Publishing),
		options:     &options,
	})
	return
}

func (b *Broker) NewProducer(opts interface{}) (err error) {
	options, ok := opts.(ProducerOptions)
	if !ok {
		return fmt.Errorf("wrong type of `producer options`, you must use `amqp.producerOptions` struct")
	}
	pName := options.ChannelOptions.Exchange
	b.producers.Store(pName, &Producer{
		publishChan: make(chan *PublishingOpts),
		options:     &options,
	})
	return
}

func (b *Broker) Serve() (err error) {
	b.inConn.maxReconnects = b.options.MaxReconnects
	b.inConn.reconnectTimeout = b.options.ReconnectTimeout
	b.outConn.maxReconnects = b.options.MaxReconnects
	b.outConn.reconnectTimeout = b.options.ReconnectTimeout
	if err = b.inConn.Open(); err != nil {
		return
	}
	log.Println("input connection opened")
	if err = b.outConn.Open(); err != nil {
		return
	}
	log.Println("output connection opened")
	b.startSentinel(b.inConn)
	b.startSentinel(b.outConn)
	if err = b.startProducers(); err != nil {
		log.Fatal(err)
	}
	if err = b.startConsumers(); err != nil {
		log.Fatal(err)
	}
	return
}

func (b *Broker) startSentinel(conn *Connection) {
	var err error
	go func() {
		var t, ioType string
		switch conn {
		case b.inConn:
			t = "consumers"
			ioType = "input"
		case b.outConn:
			t = "producers"
			ioType = "output"
		}
		amqpError := <-conn.notify
		if conn.getState() == closing {
			return
		}
		conn.setState(closed)
		log.Printf("start reconnection [%s] due to an error=[%s]", ioType, amqpError.Error())
		if err = conn.Reconnect(); err != nil {
			log.Printf("reconnection [%s] failed with error=[%s]", ioType, err)
			if e := syscall.Kill(syscall.Getpid(), syscall.SIGINT); e != nil {
				log.Fatal(e)
			}
			return
		}
		switch t {
		case "consumers":
			b.consumers.Range(func(name interface{}, cInterface interface{}) (ok bool) {
				consumer := cInterface.(*Consumer)
				err = consumer.Stop()
				if err != nil {
					return false
				}
				err = consumer.Start()
				return err == nil
			})
		case "producers":
			b.producers.Range(func(name interface{}, pInterface interface{}) (ok bool) {
				producer := pInterface.(*Producer)
				if err = producer.Stop(); err != nil {
					return false
				}
				if err = producer.Start(); err != nil {
					return false
				}
				return true
			})
		}
		if err != nil {
			log.Fatal(err)
		}
		log.Println(fmt.Sprintf("%s restarted", t))
		b.startSentinel(conn)
		log.Println(fmt.Sprintf("reconnection [%s] completed successfully", ioType))
	}()
}

func (b *Broker) Stop() (err error) {
	if err = b.stopConsumers(); err != nil {
		return
	}
	if err = b.inConn.Close(); err != nil {
		return
	}
	log.Println("input connection closed")
	if err = b.stopProducers(); err != nil {
		return
	}
	if err = b.outConn.Close(); err != nil {
		return
	}
	log.Println("output connection closed")
	return
}

func (b *Broker) Publish(opts interface{}) (err error) {
	options, ok := opts.(*PublishingOpts)
	if !ok {
		return fmt.Errorf("wrong type of `options`, you must use `PublishingOpts` struct")
	}
	iProducer, ok := b.producers.Load(options.Exchange)
	if ok {
		producer := iProducer.(*Producer)
		producer.publishChan <- options
		return
	}
	ch, err := b.outConn.amqp.Channel()
	if err != nil {
		return
	}
	defer func() {
		err = ch.Close()
	}()
	return ch.Publish(options.Exchange, options.RoutingKey, options.Mandatory, options.Immediate, options.Data)
}

func (b *Broker) startConsumers() (err error) {
	b.consumers.Range(func(name interface{}, cInterface interface{}) (ok bool) {
		consumer := cInterface.(*Consumer)
		consumer.conn = b.inConn
		err = consumer.pool.Start()
		if err != nil {
			return false
		}
		err = consumer.Start()
		return err == nil
	})
	if err != nil {
		log.Println("consumers started")
	}
	return
}

func (b *Broker) startProducers() (err error) {
	b.producers.Range(func(name interface{}, pInterface interface{}) (ok bool) {
		producer := pInterface.(*Producer)
		producer.conn = b.outConn
		err = producer.Start()
		return err == nil
	})
	if err != nil {
		log.Println("producers started")
	}
	return
}

func (b *Broker) stopConsumers() (err error) {
	b.consumers.Range(func(name interface{}, cInterface interface{}) (ok bool) {
		consumer := cInterface.(*Consumer)
		consumer.pool.Stop()
		err = consumer.Stop()
		return err == nil
	})
	if err == nil {
		log.Println("consumers stopped")
	}
	return
}

func (b *Broker) stopProducers() (err error) {
	b.producers.Range(func(name interface{}, pInterface interface{}) (ok bool) {
		producer := pInterface.(*Producer)
		err = producer.Stop()
		return err == nil
	})
	if err == nil {
		log.Println("producers stopped")
	}
	return
}
