package amqp

import (
	"fmt"
	"github.com/streadway/amqp"
	"log"
	"sync"
)

type Producer struct {
	sync.RWMutex
	conn        *Connection
	channel     *amqp.Channel
	publishChan chan *PublishingOpts
	options     *ProducerOptions
	notifyClose chan *amqp.Error
	state       int
}

func (p *Producer) Start() (err error) {
	channel, err := p.conn.amqp.Channel()
	if err != nil {
		return
	}
	if err = channel.ExchangeDeclare(
		p.options.ChannelOptions.Exchange,
		p.options.ChannelOptions.ExchangeType,
		p.options.ChannelOptions.Durable,
		p.options.ChannelOptions.AutoDelete,
		p.options.ChannelOptions.Internal,
		p.options.ChannelOptions.NoWait,
		p.options.ChannelOptions.Args,
	); err != nil {
		return fmt.Errorf("error while exchange declare [%s]", err)
	}
	if p.options.QosOptions != nil {
		err = channel.Qos(p.options.QosOptions.PrefetchCount, p.options.QosOptions.PrefetchSize, p.options.QosOptions.Global)
		if err != nil {
			return fmt.Errorf("error while setting `qos` [%s]", err)
		}
	}
	p.channel = channel
	p.setState(opened)
	p.notifyClose = make(chan *amqp.Error)
	stopChan := p.startProduce()
	p.channelWatcher(stopChan)
	return
}

func (p *Producer) Stop() (err error) {
	if p.getState() == opened {
		err = p.channel.Close()
	}
	return
}

func (p *Producer) channelWatcher(stopChan chan bool) {
	go func() {
		<-p.channel.NotifyClose(p.notifyClose)
		stopChan <- true
		<-stopChan
		close(stopChan)
		log.Println("producer stopped")
		p.setState(closed)
	}()
}

func (p *Producer) Publish(opts *PublishingOpts) error {
	return p.channel.Publish(opts.Exchange, opts.RoutingKey, opts.Mandatory, opts.Immediate, opts.Data)
}

func (p *Producer) startProduce() (stopChan chan bool) {
	stopChan = make(chan bool)
	go func() {
	Loop:
		for {
			select {
			case pub := <-p.publishChan:
				if e := p.Publish(pub); e != nil {
					log.Println(e)
				}
			case <-stopChan:
				break Loop
			}
		}
		stopChan <- true
	}()
	return stopChan
}

func (p *Producer) getState() int {
	p.RLock()
	defer p.RUnlock()
	return p.state
}

func (p *Producer) setState(state int) {
	p.Lock()
	p.state = state
	p.Unlock()
}
