package broker

import (
	"bitbucket.org/moelius/qrpc/concurrency"
	"time"
)

type Option func(options *Options)

type Options struct {
	URL              string
	MaxReconnects    int
	ReconnectTimeout time.Duration
}

var DefaultOptions = Options{
	URL:              "amqp://",
	MaxReconnects:    100,
	ReconnectTimeout: 10 * time.Second,
}

func URL(url string) Option {
	return func(o *Options) {
		o.URL = url
	}
}

func MaxReconnects(max int) Option {
	return func(o *Options) {
		o.MaxReconnects = max
	}
}

func ReconnectTimeout(timeout time.Duration) Option {
	return func(o *Options) {
		o.ReconnectTimeout = timeout
	}
}

type Broker interface {
	Configure(Options) error
	Serve() error
	Stop() error
	Publish(opts interface{}) error
	NewConsumer(opts interface{}, pool *concurrency.Pool) error
	NewProducer(opts interface{}) error
}

func Configure(broker Broker, options Options) error {
	return broker.Configure(options)
}

func Serve(broker Broker) error {
	return broker.Serve()
}

func Stop(broker Broker) error {
	return broker.Stop()
}

func NewConsumer(broker Broker, opts interface{}, pool *concurrency.Pool) error {
	return broker.NewConsumer(opts, pool)
}

func NewProducer(broker Broker, opts interface{}) error {
	return broker.NewProducer(opts)
}

func Publish(broker Broker, opts interface{}) error {
	return broker.Publish(opts)
}
