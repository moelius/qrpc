package qrpc

import (
	"bitbucket.org/moelius/qrpc/broker"
	"bitbucket.org/moelius/qrpc/broker/amqp"
	"fmt"
	"log"
	"os"
	"os/signal"
	"strings"
	"sync"
	"syscall"
)

type Server struct {
	broker   broker.Broker
	registry []*RPC
}

func NewServer(brokerOptions ...broker.Option) (server *Server, err error) {
	opts := broker.DefaultOptions
	for _, o := range brokerOptions {
		o(&opts)
	}
	server = new(Server)
	if strings.HasPrefix(opts.URL, "amqp://") {
		server.broker = amqp.NewBroker()
		if err = server.broker.Configure(opts); err != nil {
			return
		}
		return
	}
	err = fmt.Errorf("unknown broker url type: [%s]", opts.URL)
	return
}

func (server *Server) ListenAndServe() (err error) {
	if err = server.Serve(); err != nil {
		return
	}
	server.SignalHandler()
	return
}

func (server *Server) Serve() error {
	return broker.Serve(server.broker)
}

func (server *Server) Stop() error {
	if err := broker.Stop(server.broker); err != nil {
		return err
	}
	return nil
}

func (server *Server) SignalHandler() {
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
	wg := sync.WaitGroup{}
	wg.Add(1)
	go func() {
		<-sig
		if err := server.Stop(); err != nil {
			log.Fatal(err)
		}
		wg.Done()
	}()
	wg.Wait()
}
